name: title
class: center, middle
# JavaScript
---
name: table-of-contents
class: middle
# Table of Contents:
1. Introduction
2. Data Types in JavaScript
3. Variables and Assignments
4. Try It Yourself: Using the Browser's JS Console
5. Syntax Structure
6. Functions
7. Operators and Comparison
8. Control Structures: Conditional and Loops
9. Objects and Arrays
10. Embedding JS in HTML
---
name: introduction
class: middle
# 1. Introduction
#### Javascript Example
```javascript
var a;
var b = 2;
var c = 'string';

a = 1;
console.log(a + b + c);
console.log(a + (b + c));

// 3string
// 12string
```
---
name: data-types
class: middle
# 2. Data Types in JavaScript
#### Number
```javascript
1
3.24
133e20
```
#### String
```javascript
'hello'
"world"
`some text`
'something\nnewline\nseparated'
```
#### Boolean
```javascript
true
false
```
???
Numbers are numbers
---
#### Object
```javascript
{
  time: 'party'
}
```
#### Null & Undefined
```javascript
null
undefined
```
---
name: variables-and-assignments
class: middle
# 3. Variables and Assignments
#### Declaring a variable
```javascript
var a;
```
#### Assigning a value to a variable
```javascript
a = 3;
```
#### Declaring and assigning a variable
```javascript
var a = 3;
```
---
name: try-it-yourself
#Try It Yourself: Using the Browser's JS Console
![alt text][console]
[console]: images/console.png "Image of Console"
